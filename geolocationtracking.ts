class GeoTracker {
    id: number;
    position_change_callback: PositionCallback;
    position_change_error_callback?: PositionErrorCallback;
    constructor(callback: PositionCallback, error_callback?: PositionErrorCallback) {
        if (error_callback != null) {
            this.position_change_error_callback = error_callback;
        }
        this.position_change_callback = callback
        this.id = 0
        this.init()
    }
    init() {
     if (navigator.geolocation) {
            if (this.position_change_error_callback != null) {
                this.id = navigator.geolocation.watchPosition(this.position_change_callback, this.position_change_error_callback)
            } 
            else {
                this.id = navigator.geolocation.watchPosition(this.position_change_callback)
            }
        }
        else {
            throw new Error("No support for required GPS functions in client")
        }
    }
    drop() {
        navigator.geolocation.clearWatch(this.id)
    }

    withErrorCallback(error_callback: PositionCallback) {
        this.position_change_callback = error_callback
        this.drop()
        this.init()
    }

    reinitialise() {
        this.drop()
        this.init()
    }
}