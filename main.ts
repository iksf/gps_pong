
function geoLocation () {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            (pos) => {
                let coord = pos.coords
                console.log(`lat: ${coord.latitude} lng: ${coord.longitude} acc: ${coord.accuracy}`)
            }
        )
    }
    else {
        console.log(`geolocation not supported`)
    }
}




geoLocation()

