"use strict";
function geoLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (pos) {
            var coord = pos.coords;
            console.log("lat: " + coord.latitude + " lng: " + coord.longitude + " acc: " + coord.accuracy);
        });
    }
    else {
        console.log("geolocation not supported");
    }
}
geoLocation();
